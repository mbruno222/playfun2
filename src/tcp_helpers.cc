/*
 * tcp_helpers.cc
 *
 *  Created on: Mar 28, 2015
 *      Author: mbruno
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <unistd.h>



int recv_exactly(int sock, void *buf, ssize_t len)
{
	ssize_t len_left = len;
	ssize_t len_last;

	while (len_left > 0) {
		len_last = recv(sock, ((uint8_t *)buf)+len-len_left, len_left, 0);
		if (len_last <= 0) {
			return -1;
		}
		len_left -= len_last;
	}

	return 0;
}


/* Begin listening on port `port' on all interfaces.
 * Returns socket descriptor of the listening socket.
 */
int start_listening(short port)
{
	struct sockaddr_in addr;
	int lsock, on = 1;

	lsock = socket(PF_INET, SOCK_STREAM, 0);

	if (lsock == -1) {
		perror("Socket Creation Failed");
		return -1;
	}

	setsockopt(lsock, SOL_SOCKET, SO_REUSEADDR, (void *) &on, sizeof(int));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	memset(&(addr.sin_zero), 0, sizeof(addr.sin_zero));

	if (bind(lsock, (struct sockaddr *) &addr, sizeof(struct sockaddr)) == -1) {
		perror("Bind Failed");
		close(lsock);
		return -1;
	}

	if (listen(lsock, 0) == -1) {
		perror("Listen Failed");
		close(lsock);
		return -1;
	}

	return lsock;
}

int get_connection(short port)
{
	fd_set readfds;
	struct sockaddr_in remote_addr;
	socklen_t addrlen;
	int csock, lsock;

	lsock = start_listening(port);
	if (lsock == -1) {
		return -1;
	}

	/* Watch the listening socket */
	FD_ZERO(&readfds);
	FD_SET(lsock, &readfds);

	while (1) {
		/* Block waiting for an incoming connection */
		select(lsock+1, &readfds, NULL, NULL, NULL);

		addrlen = sizeof(remote_addr);
		csock = accept(lsock, (struct sockaddr *) &remote_addr, &addrlen);
		if (csock == -1) {
			perror("Accept Failed");
			continue;
		}

		/*
		 * Stop listening
		 */
		close(lsock);

		return csock;
	}
}

int open_tcp_connection(const char *hostname, const char *port)
{
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	char *hostname_noport, *hostname_port;
	int error;
	int sock;

	hostname_noport = strdup(hostname);

	/*
	 * If there is a colon in the hostname
	 * replace it with a null and set the
	 * port string to start at the first char
	 * after the colon, overriding the port
	 * parameter
	 */
	hostname_port = strchr(hostname_noport, (int) ':');
	if (hostname_port != NULL) {
		hostname_port[0] = '\0';
		port = &hostname_port[1];
	}

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	error = getaddrinfo(hostname_noport, port, &hints, &result);
	free(hostname_noport);

	if (error != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return -1;
	}

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (sock == -1) {
			continue;
		}

		if (connect(sock, rp->ai_addr, rp->ai_addrlen) == 0) {
			break; /* Success */
		}

		close(sock);
	}

	if (rp == NULL) { /* No address succeeded */
		fprintf(stderr, "Could not connect\n");
		return -1;
	}

	freeaddrinfo(result); /* No longer needed */

	return sock;
}
