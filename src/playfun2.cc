/*
 * playfun2.cc
 *
 *  Created on: Mar 21, 2015
 *      Author: mbruno
 */

#include <vector>
#include <string>
#include <limits.h>

#include <signal.h>

#include "nes_emulation/simplefm2.h"
#include "nes_emulation/emulator.h"

#include "playfun2.h"
#include "playfun2_net.h"
#include "tcp_helpers.h"

//    RLDUTSBA (Right, Left, Down, Up, sTart, Select, B, A)
#define BTN_RIGHT  0X80
#define BTN_LEFT   0X40
#define BTN_DOWN   0X20
#define BTN_UP     0X10
#define BTN_START  0X08
#define BTN_SELECT 0X04
#define BTN_B      0X02
#define BTN_A      0X01

#define BTN_COMBO_1  0
#define BTN_COMBO_2  BTN_RIGHT
#define BTN_COMBO_3  BTN_RIGHT|BTN_A
#define BTN_COMBO_4  BTN_RIGHT|BTN_B
#define BTN_COMBO_5  BTN_RIGHT|BTN_A|BTN_B
#define BTN_COMBO_6  BTN_LEFT
#define BTN_COMBO_7  BTN_LEFT|BTN_A
#define BTN_COMBO_8  BTN_LEFT|BTN_B
#define BTN_COMBO_9  BTN_LEFT|BTN_A|BTN_B
#define BTN_COMBO_10 BTN_DOWN
#define BTN_COMBO_11 BTN_DOWN|BTN_RIGHT
#define BTN_COMBO_12 BTN_DOWN|BTN_LEFT
#define BTN_COMBO_13 BTN_A
#define BTN_COMBO_14 BTN_B

typedef struct {
	timer_t timer_id;
	sigset_t alarm_sig;
} my_timer_t;

vector<uint8> get_btn_combos()
{

	vector<uint8> out;

	out.push_back(BTN_COMBO_1);
//	out.push_back(BTN_COMBO_2);
//	out.push_back(BTN_COMBO_3);
	out.push_back(BTN_COMBO_4);
	out.push_back(BTN_COMBO_5);
//	out.push_back(BTN_COMBO_6);
//	out.push_back(BTN_COMBO_7);
	out.push_back(BTN_COMBO_8);
	out.push_back(BTN_COMBO_9);
	out.push_back(BTN_COMBO_10);
//	out.push_back(BTN_COMBO_11);
//	out.push_back(BTN_COMBO_12);
	out.push_back(BTN_COMBO_13);
	out.push_back(BTN_COMBO_14);

	return out;
}

int mod(int a, int b)
{
	int r = a % b;
	return r >= 0 ? r : r + b;
}

static int create_timer(my_timer_t *timer, time_t ns_period)
{
	time_t ns;
	time_t sec;
	struct sigevent sigev;
	struct itimerspec itval;

	/* Add SIGRTMIN to the signal set */
	sigemptyset(&timer->alarm_sig);
	sigaddset(&timer->alarm_sig, SIGRTMIN);
	sigprocmask(SIG_BLOCK, &timer->alarm_sig, NULL);

	/* Create a timer that will generate the signal SIGRTMIN */
	sigev.sigev_notify = SIGEV_SIGNAL;
	sigev.sigev_signo = SIGRTMIN;
	sigev.sigev_value.sival_ptr = (void *) &timer->timer_id;
	if (timer_create(CLOCK_MONOTONIC, &sigev, &timer->timer_id) == -1) {
		return -1;
	}

	/* Make the timer periodic */
	sec = ns_period / 1000000000;
	ns = ns_period - (sec * 1000000000);
	itval.it_interval.tv_sec = sec;
	itval.it_interval.tv_nsec = ns;
	itval.it_value.tv_sec = sec;
	itval.it_value.tv_nsec = ns;

	return timer_settime(timer->timer_id, 0, &itval, NULL);
}

static void wait_timer(my_timer_t *timer)
{
	int sig;

	sigwait(&timer->alarm_sig, &sig);
}

static int get_time(vector<uint8> &mem)
{
	return 100*mem[0x07F8] + 10*mem[0x07F9] + mem[0x07FA];
}

static int get_score(vector<uint8> &mem)
{
	if (get_time(mem) <= 100) {
		return 0;
	} else {
		return 100000 * mem[0x07DE] + 10000 * mem[0x07DF] + 1000 * mem[0x07E0]
		       + 100 * mem[0x07E1] + 10 * mem[0x07E2];
	}
}

static int get_coins(vector<uint8> &mem)
{
	return mem[0x075E];
}

static int get_lives(vector<uint8> &mem)
{
	return mem[0x075A];
}

static int get_level(vector<uint8> &mem)
{
	return 4 * mem[0x0075F] + mem[0x075C];
}

static int get_powerup(vector<uint8> &mem)
{
	return mem[0x0756];
}

static int get_player_x(vector<uint8> &mem)
{
	return mem[0x006D] == 0xFF ? 0 : 256 * mem[0x006D] + mem[0x0086];
}

static int get_player_y(vector<uint8> &mem)
{
	/*
	 * Return y position only if swimming
	 */
	return mem[0x0704] ? -(256 * mem[0x00B5] + mem[0x00CE]) : 0;
}

static int get_state_score(vector<uint8> &mem)
{
	int vertical = mem[0x00B5];
	int player_state = mem[0x000E];
	int float_state = mem[0x001D];

	if (float_state == 3) {
		/* on flag! */
		//printf("FLAG!\n");
		return 1000000;
	}

	if (vertical > 1) {
		/* fell off screen, death */
		//printf("FALL!\n");
		return INT_MIN;
	}

	switch (player_state) {
	case 11:
	case 6:
		/* death */
		//printf("DEATH!\n");
		return INT_MIN;
		break;
	case 2:
	case 3:
		/* entering pipe */
		//printf("PIPE!\n");
		return 1000000;
		break;
	}

	/*
	 * Helps to navigate world 4-4 by avoiding
	 * walking down the paths that cause a loop.
	 */
	if (get_level(mem) == 15) {
		if (get_player_x(mem) > 0x30 && get_player_x(mem) < 0x3DF) {
			if (mem[0x00CE] > 130) {
				return INT_MIN;
			}
		} else if (get_player_x(mem) > 0x630 && get_player_x(mem) < 0x700) {
			if (mem[0x00CE] < 150) {
				return INT_MIN;
			}
		}
	}

	return 0;
}

#define GOAL 3

int get_overall_score(vector<uint8> &mem)
{
	int score = 0;

#if GOAL == 1
	score += 10 * get_score(mem);
	score += 100 * get_coins(mem);
	score += 20 * get_lives(mem);
	score += 1000 * get_level(mem);
	score += 100 * get_powerup(mem);
	score += get_player_x(mem);
	score += get_player_y(mem);
#elif GOAL == 2
	score += get_score(mem);
	score += get_coins(mem);
	score += 1000*get_lives(mem);
	score += 10*get_level(mem);
	score += 1000*get_powerup(mem);
	score += 10*get_player_x(mem);
	score += get_player_y(mem);
#elif GOAL == 3
	score += get_player_x(mem);
	score += get_player_y(mem);
#endif

	return score;
}

static void print_status(vector<uint8> &mem)
{
	int time =  get_time(mem);
	int score = get_score(mem);
	int coins = get_coins(mem);
	int lives = get_lives(mem);
	int level = get_level(mem);
	int powerup = get_powerup(mem);
	int player_x = get_player_x(mem);
	int player_y = get_player_y(mem);
	int state_score = get_state_score(mem);
	int overall_score = get_overall_score(mem);

//	printf("\033[2J");
	printf("Time:     %d\n", time);
	printf("Score:    %d\n", score);
	printf("Coins:    %d\n", coins);
	printf("Lives:    %d\n", lives);
	printf("Level:    %d\n", level);
	printf("Powerup:  %d\n", powerup);
	printf("Level X: %04X\n", player_x);
	printf("Level Y: %04X\n", player_y);
	printf("Screen Y: %d\n", mem[0x00CE]);
	printf("State score: %d\n", state_score);
	printf("Overall Score:  %d\n", overall_score);
}

static void hold_buttons(uint8 buttons, int caching)
{
	int i;

	for (i = 0; i < BTN_HOLD_LEN; i++) {
		caching ? Emulator::CachingStep(buttons) : Emulator::Step(buttons);
	}
}

static void execute_next(future_t *f, vector<uint8> *steps, int caching)
{
	int i, j;

	for (i = 0; i < NEXT_LEN; i++) {
		uint8 input = f->steps[mod((f->next_index + i), FUTURE_LEN)];

		hold_buttons(input, caching);

		if (steps != NULL) {
			for (j = 0; j < BTN_HOLD_LEN; j++) {
				steps->push_back(input);
			}
		}
	}
}

static int get_future_tail_score(future_t *f)
{
	int i;
	int score;
	vector<uint8> memory;
	vector<uint8> savestate;

	Emulator::SaveUncompressed(&savestate);

	for (i = 0; i < FUTURE_LEN - NEXT_LEN; i++) {
		uint8 input = f->steps[mod((f->tail_index + i), FUTURE_LEN)];

		hold_buttons(input, 1);

		/*
		 * At every step check for events that make what happens
		 * next not really matter (death, win, etc).
		 */
		Emulator::GetMemory(&memory);
		score = get_state_score(memory);
		if (score < 0) {
			Emulator::LoadUncompressed(&savestate);
			return score;
		} else if (score > 0) {
			Emulator::LoadUncompressed(&savestate);
			return (FUTURE_LEN - NEXT_LEN - i) * score;
		}
	}

	score = get_overall_score(memory);

	Emulator::LoadUncompressed(&savestate);

	return score;
}

static void create_random_sequence(future_t *f, vector<uint8> &input_combos,
                                   int index, int len)
{
	int i;

	uint8 rand;

	for (i = 0; i < len; i++) {
		rand = input_combos[mod(random(), input_combos.size())];
		f->steps[mod((index + i), FUTURE_LEN)] = rand;
	}
}

static void shuffle_futures_column(future_t *fs, int col)
{
	int i;

	for (i = NUM_FUTURES-1; i > 0; i--) {
		int j = mod(random(), (i+1));
		uint8 temp = fs[i].steps[col];
		fs[i].steps[col] = fs[j].steps[col];
		fs[j].steps[col] = temp;
	}
}

static void extend_futures(future_t *fs, vector<uint8> &input_combos, int index,
                           int len)
{
	int i, j;

	for (i = 0; i < len; i++) {

		int col = mod((index + i), FUTURE_LEN);

		for (j = 0; j < NUM_FUTURES; j++) {
			fs[j].steps[col] = input_combos[mod(j, input_combos.size())];
		}

		shuffle_futures_column(fs, col);
	}
}

static void extend_and_shift_futures_orig(future_t *fs, vector<uint8> &input_combos)
{
	int i;

	for (i = 0; i < NUM_FUTURES; i++) {
		fs[i].tail_index = mod((fs[i].tail_index + NEXT_LEN), FUTURE_LEN);
		create_random_sequence(&fs[i], input_combos, fs[i].next_index, NEXT_LEN);
		fs[i].next_index = mod((fs[i].next_index + NEXT_LEN), FUTURE_LEN);
	}
}

static void extend_and_shift_futures_shuffle(future_t *fs, vector<uint8> &input_combos)
{
	int i;

	/*
	 * Next index should be pointing at the next that was just executed.
	 * This will become the end of the tail, so "extend" these tails
	 * with random inputs. Also replace the first inputs of all the nexts
	 * (hence the +1) to ensure that each button combo is available to
	 * choose from.
	 */
	extend_futures(fs, input_combos, fs[0].next_index, NEXT_LEN+1);

	for (i = 0; i < NUM_FUTURES; i++) {
		fs[i].tail_index = mod((fs[i].tail_index + NEXT_LEN), FUTURE_LEN);
		fs[i].next_index = mod((fs[i].next_index + NEXT_LEN), FUTURE_LEN);
	}
}

static void undo_last_future_shift(future_t *fs)
{
	int i;

	printf("Undoing last shift\n");

	/*
	 * Move the pointers back by NEXT_LEN
	 */
	for (i = 0; i < NUM_FUTURES; i++) {
		fs[i].tail_index = mod((fs[i].tail_index - NEXT_LEN), FUTURE_LEN);
		fs[i].next_index = mod((fs[i].next_index - NEXT_LEN), FUTURE_LEN);
	}
}

static next_score_t get_future_tails_max_score(future_t *fs)
{
	int i;
	next_score_t max_score;

	max_score.score = INT_MIN;
	max_score.associated_future = 0;


	//printf("\033[2J");
	for (i = 0; i < NUM_FUTURES; i++) {
		int score = get_future_tail_score(&fs[i]);
		//printf("Future %d: %d\n", i, score);
		if (score > max_score.score) {
			max_score.score = score;
			max_score.associated_future = i;
		}
	}

	return max_score;
}

next_score_t score_next(future_t *fs, int future_number)
{
	vector<uint8> savestate;

	Emulator::SaveUncompressed(&savestate);

	execute_next(&fs[future_number], NULL, 1);

	fs[future_number].score = get_future_tails_max_score(fs);

	Emulator::LoadUncompressed(&savestate);

	return fs[future_number].score;
}
#if 0
static int score_all_futures(future_t *fs)
{
	int i;
	int best_future = 0;
	int max_score = 0;
	int current_score;

	vector<uint8> memory;

	Emulator::GetMemory(&memory);
	current_score = get_overall_score(memory);

	for (i = 0; i < NUM_FUTURES; i++) {
		int score = score_next(fs, i, current_score);
		if (score > max_score) {
			max_score = score;
			best_future = i;
		}
	}

	return best_future;
}
#endif

static int sockets[CHILD_COUNT];

static int execute_best_next(future_t *fs, vector<uint8> &input_combos,
                              vector<uint8> *inputs)
{
	int best_next;

	//best_future = score_all_futures(fs);
	best_next = score_all_nexts_net(fs, sockets, CHILD_COUNT);

	if (best_next == -1) {
		return -1;
	}

	if (best_next == -2) {
		printf("No good nexts found\n");
		return -2;
	}

	printf("Executing best next %d: score is %d, future is %d\n", best_next, fs[best_next].score.score, fs[best_next].score.associated_future);

	execute_next(&fs[best_next], inputs, 0);

	return best_next;
}

static void copy_future_sequence(future_t *fs, int dest, int src)
{
	memcpy(fs[dest].steps, fs[src].steps, FUTURE_LEN);
}

static void mutate_future_sequence(future_t *fs, vector<uint8> &input_combos, int future_number)
{
	int i;

	for (i = 0; i < FUTURE_LEN; i++) {
		if (mod(random(), 10) == 0) {
			fs[future_number].steps[i] = input_combos[mod(random(), input_combos.size())];
		}
	}
}

static void replace_poor_futures(future_t *fs, vector<uint8> &input_combos, int best_next)
{
	int i;
	int mutate = 1;

	/*
	 * Mark all futures for replacement
	 */
	for (i = 0; i < NUM_FUTURES; i++) {
		//printf("Next %d: score is %d, future is %d\n", i, fs[i].score.score, fs[i].score.associated_future);
		fs[i].replace = 1;
	}

	/*
	 * Now unmark those futures that are the best for any next
	 */
	for (i = 0; i < NUM_FUTURES; i++) {
		fs[fs[i].score.associated_future].replace = 0;
	}

	/*
	 * Replace every other worst futures with a mutated version
	 * of the best future and the rest with a completely random future
	 */
	for (i = 0; i < NUM_FUTURES; i++) {
		if (fs[i].replace) {
			//printf("Will replace future %d with %d\n", i, fs[best_next].score.associated_future);
#if 1
			copy_future_sequence(fs, i, fs[best_next].score.associated_future);
			mutate_future_sequence(fs, input_combos, i);
#else
			if (mutate) {
				copy_future_sequence(fs, i, fs[best_next].score.associated_future);
				mutate_future_sequence(fs, input_combos, i);
			} else {
				create_random_sequence(&fs[i], input_combos, 0, FUTURE_LEN);
			}
			mutate = !mutate;
#endif
		}
	}
}

#define USE_SHUFFLE_EXTENSION 1

static void reset_all_futures(future_t *fs, vector<uint8> &input_combos)
{
	int i;

	printf("Resetting futures\n");

	for (i = 0; i < NUM_FUTURES; i++) {
#if !USE_SHUFFLE_EXTENSION
		create_random_sequence(&futures[i], input_combos, 0, FUTURE_LEN);
#endif
		fs[i].next_index = 0;
		fs[i].tail_index = NEXT_LEN;
	}

#if USE_SHUFFLE_EXTENSION
	extend_futures(fs, input_combos, 0, FUTURE_LEN);
#endif
}

static void playfun(string game, string out_movie, vector<uint8> *inputs, vector<uint8> *savestate)
{
	int i;
	int retry_tail_extension_count = 0;
	int retry_future_reset_count = 0;
	vector<uint8> input_combos;
	future_t futures[NUM_FUTURES];

	input_combos = get_btn_combos();

	reset_all_futures(futures, input_combos);

	for (i = 0;; i++) {
		int best_next;

		best_next = execute_best_next(futures, input_combos, inputs);
		if (best_next == -1) {
			return;
		} else if (best_next == -2) {
			if (retry_tail_extension_count++ < 10) {
				undo_last_future_shift(futures);
			} else if (retry_future_reset_count++ < 20) {
				reset_all_futures(futures, input_combos);
			} else {
				/*
				 * Give up...
				 * execute_best_next() did not actually execute
				 * any next, so just execute the first one now
				 */
				execute_next(&futures[0], inputs, 0);
			}
		} else {
			retry_tail_extension_count = 0;
			retry_future_reset_count = 0;
			replace_poor_futures(futures, input_combos, best_next);
		}

#if USE_SHUFFLE_EXTENSION
		extend_and_shift_futures_shuffle(futures, input_combos);
#else
		extend_and_shift_futures_orig(futures, input_combos);
#endif

		if (mod(i, 10) == 0) {
			vector<uint8> memory;
			Emulator::GetMemory(&memory);
			print_status(memory);
			SimpleFM2::WriteInputsWithSavestate(out_movie, game,
			                                    "base64:jjYwGG411HcjG/j9UOVM3Q==", *inputs, *savestate);
		}
	}
}

int main(int argc, char **argv)
{
	pid_t pid;
	int i;

	vector<uint8> try_inputs;
	vector<uint8> inputs;
	vector<uint8> memory;
	vector<uint8> savestate;

	std::string game;
	std::string out_movie;
	std::string start_movie;

	if (argc < 2) {
		return 0;
	}

	game = string(argv[1]) + ".nes";

	if (argc > 2) {
		out_movie = string(argv[2]) + ".fm2";
	} else {
		out_movie = "movie.fm2";
	}

	if (argc > 3) {
		start_movie = argv[3];
	}

	Emulator::Initialize(game);

	try_inputs = get_btn_combos();

	for (i = 0; i < CHILD_COUNT; i++) {
		pid = fork();
		if (pid == 0) {
			break;
		}
		printf("Made child with pid %d\n", pid);
	}

	if (pid == 0) {
		int sock;

		Emulator::ResetCache(10000, 1000);

		sock = get_connection(8000 + i);

		playfun_net(sock);

		return 0;

	} else {

		for (i = 0; i < CHILD_COUNT; i++) {
			char port_str[5];

			snprintf(port_str, sizeof(port_str), "%d", 8000 + i);

			sockets[i] = open_tcp_connection("localhost", port_str);
			if (sockets[i] == -1) {
				return -1;
			}
		}
	}

#define PLAY_REALTIME 0

	if (start_movie.size() != 0) {

		/*
		 * If an input movie was provided, load its savestate if
		 * there is one and then play until the end. Then save the
		 * state. That state will be saved out to the resulting output
		 * movie file and play will begin there.
		 */

		inputs = SimpleFM2::ReadInputs(start_movie + ".fm2", savestate);

		if (savestate.size() != 0) {
			if (!Emulator::LoadState(&savestate)) {
				printf("Failed to load state\n");
				return -1;
			}
		}

	#if PLAY_REALTIME
		my_timer_t timer;
		create_timer(&timer, 16666667);
	#endif

		for (i = 0; i < (int) inputs.size(); i++) {

	#if PLAY_REALTIME
			wait_timer(&timer);
	#endif

			Emulator::Step(inputs[i]);
	#if PLAY_REALTIME
			Emulator::GetMemory(&memory);

			print_status(memory);
	#endif
		}

		inputs.resize(0);    // discard all the movie inputs
		savestate.resize(0); // discard the movie savestate
		Emulator::SaveState(&savestate); // and get the current save state

	} else {

		/*
		 * If there is no input movie, then just try
		 * to start the game here. No save state will
		 * be created and play will begin after a hard
		 * reset.
		 */

		for (i = 0; i < 60 * 2; i++) {
			Emulator::Step(0);
			inputs.push_back(0);
		}
		Emulator::Step(BTN_START);
		inputs.push_back(BTN_START);
		for (i = 0; i < 60 * 3; i++) {
			Emulator::Step(0);
			inputs.push_back(0);
		}
	}

	srandom(time(NULL));

	//Emulator::ResetCache(100000, 10000);

	playfun(game, out_movie, &inputs, &savestate);


	return 0;
}
