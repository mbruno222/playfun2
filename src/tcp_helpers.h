/*
 * tcp_helpers.h
 *
 *  Created on: Mar 28, 2015
 *      Author: mbruno
 */

#ifndef SRC_TCP_HELPERS_H_
#define SRC_TCP_HELPERS_H_

int recv_exactly(int sock, void *buf, ssize_t len);
int start_listening(short port);
int get_connection(short port);
int open_tcp_connection(const char *hostname, const char *port);

#endif /* SRC_TCP_HELPERS_H_ */
