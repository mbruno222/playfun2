/*
 * playfun_net.h
 *
 *  Created on: Mar 28, 2015
 *      Author: mbruno
 */

#ifndef SRC_PLAYFUN2_NET_H_
#define SRC_PLAYFUN2_NET_H_

typedef struct {
	int start_index;
	int eval_count;
	future_t futures[NUM_FUTURES];
} future_eval_request_t;

void playfun_net(int sock);
int score_all_nexts_net(future_t *fs, int sockets[], int socket_count);

#endif /* SRC_PLAYFUN2_NET_H_ */
