/*
 * playfun2_net.cc
 *
 *  Created on: Mar 28, 2015
 *      Author: mbruno
 */

#include <vector>
#include <string>
#include <limits.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "nes_emulation/emulator.h"


#include "tcp_helpers.h"
#include "playfun2.h"
#include "playfun2_net.h"

static int send_buffer(int sock, void *buf, ssize_t len)
{
	if (send(sock, &len, sizeof(ssize_t), 0) != sizeof(ssize_t)) {
		return -1;
	}
	if (send(sock, buf, len, 0) != len) {
		return -1;
	}

	return 0;
}

static void *recv_buffer(int sock, ssize_t *len)
{
	ssize_t len_i;
	uint8_t *buf;

	*len = 0;

	if (recv_exactly(sock, &len_i, sizeof(ssize_t)) == -1) {
		return NULL;
	}

	buf = (uint8_t *) malloc(len_i);

	if (recv_exactly(sock, buf, len_i) == -1) {
		free(buf);
		return NULL;
	}

	*len = len_i;
	return buf;
}

static int send_byte_vector(int sock, vector<uint8_t> &v)
{
	return send_buffer(sock, &v[0], v.size());
}

static int recv_byte_vector(int sock, vector<uint8_t> &v)
{
	ssize_t len_i;

	if (recv_exactly(sock, &len_i, sizeof(ssize_t)) == -1) {
		return -1;
	}

	v.resize(len_i);

	if (recv_exactly(sock, &v[0], len_i) == -1) {
		return -1;
	}

	return 0;
}

void playfun_net(int sock)
{
	vector<uint8> savestate;
	future_eval_request_t *future_req;
	ssize_t future_req_len;
	int i;
	next_score_t scores[NUM_FUTURES];

	for (;;) {

		/*
		 * First receive the current game state from the
		 * master node
		 */
		if (recv_byte_vector(sock, savestate) == -1) {
			printf("fail to recv state\n");
			return;
		}
		Emulator::LoadUncompressed(&savestate);

		/*
		 * Now receive the request with all the futures
		 * and which ones to evaluate
		 */
		future_req = (future_eval_request_t *) recv_buffer(sock, &future_req_len);
		if (future_req == NULL) {
			printf("fail to receive future request\n");
			return;
		}

		if (future_req_len != sizeof(future_eval_request_t)) {
			printf("Received invalid future request size\n");
			free(future_req);
			return;
		}

		/*
		 * Evaluate them and save each one's tail score into an
		 * array
		 */
		//printf("Evaluating %d futures from index %d\n", future_req->eval_count, future_req->start_index);
		for (i = 0; i < future_req->eval_count; i++) {
			scores[i] = score_next(future_req->futures, future_req->start_index+i);
		}

		/*
		 * Send the score array back to the master node
		 */
		if (send_buffer(sock, scores, sizeof(next_score_t) * future_req->eval_count) == -1) {
			printf("fail to send scores\n");
			free(future_req);
			return;
		}

		free(future_req);
	}
}

int score_all_nexts_net(future_t *fs, int sockets[], int socket_count)
{
	int i;
	int best_next = -2; // -2 is not error but all nexts are bad
	int max_score = INT_MIN;
	int current_score;
	int future_count_min_per_node;
	int extra_futures_counts;
	int future_start_index = 0;
	future_eval_request_t future_req;

	vector<uint8> memory;
	vector<uint8> savestate;

	/*
	 * First get the score at the current game state
	 */
	Emulator::GetMemory(&memory);
	current_score = get_overall_score(memory);

	/*
	 * Now save the current game state...
	 */
	Emulator::SaveUncompressed(&savestate);

	/*
	 * ...and send it to each of the remote nodes
	 */
	for (i=0; i<socket_count; i++) {
		if (send_byte_vector(sockets[i], savestate) == -1) {
			printf("fail to send state\n");
			return -1;
		}
	}

	/*
	 * Decide how many futures to have each node evaluate.
	 * If NUM_FUTURES is not a multiple of the number of
	 * nodes then we distribute the remainder.
	 */
	future_count_min_per_node = NUM_FUTURES / socket_count;
	extra_futures_counts = NUM_FUTURES % socket_count;

	/*
	 * Copy all the futures into the request that will
	 * be sent to all the nodes
	 */
	memcpy(future_req.futures, fs, sizeof(future_req.futures));

	/*
	 * Now send all the futures and the starting index and number
	 * of futures to evaluate to each node
	 */
	for (i = 0; i < socket_count; i++) {

		int future_count;

		future_count = future_count_min_per_node;
		if (extra_futures_counts > 0) {
			future_count++;
			extra_futures_counts--;
		}

		future_req.start_index = future_start_index;
		future_req.eval_count = future_count;

		future_start_index += future_count;

		if (send_buffer(sockets[i], &future_req, sizeof(future_req)) == -1) {
			printf("fail to send futures\n");
			return -1;
		}
	}

	extra_futures_counts = NUM_FUTURES % socket_count;
	future_start_index = 0;

	/*
	 * Now receive the scores back from each node
	 */
	for (i=0; i<socket_count; i++) {

		int j;
		int future_count;
		ssize_t len;
		next_score_t *scores;

		future_count = future_count_min_per_node;
		if (extra_futures_counts > 0) {
			future_count++;
			extra_futures_counts--;
		}

		scores = (next_score_t *) recv_buffer(sockets[i], &len);
		if (scores == NULL) {
			printf("fail to receive scores\n");
			return -1;
		}

		if (len != (ssize_t) sizeof(next_score_t) * future_count) {
			printf("Unexpected number of future scores received!\n");
			free(scores);
			return -1;
		}

		for (j = 0; j < future_count; j++) {

			int score;

			/*
			 * Subtract the current score from the next score
			 * to get a score relative to where we are now. Don't
			 * subtract if INT_MIN or INT_MAX
			 */
			if (scores[j].score != INT_MIN) {
				scores[j].score -= current_score;
			}

			score = scores[j].score;

			if (scores[j].associated_future < 0 || scores[j].associated_future > NUM_FUTURES-1) {
				printf("Reported associated future %d for next %d is out of range!\n", scores[j].associated_future, i);
				return -1;
			}

			fs[future_start_index + j].score = scores[j];

			if (score > max_score) {
				max_score = score;
				best_next = future_start_index + j;
			}
		}

		future_start_index += future_count;

		free(scores);
	}

	return best_next;
}

