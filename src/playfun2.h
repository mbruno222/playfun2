/*
 * playfun2.h
 *
 *  Created on: Mar 28, 2015
 *      Author: mbruno
 */

#ifndef SRC_PLAYFUN2_H_
#define SRC_PLAYFUN2_H_



#define CHILD_COUNT 8

#define SPEED 1

#if SPEED==0
#define NUM_FUTURES    16
#define BTN_HOLD_LEN   12
#define FUTURE_LEN     (4*5)
#define NEXT_LEN       (3)
#elif SPEED==1
#define NUM_FUTURES    8
#define BTN_HOLD_LEN   6      // 10 times per second
#define FUTURE_LEN     (4*10) // 4 seconds
#define NEXT_LEN       (3)
#elif SPEED==2
#define NUM_FUTURES    32
#define BTN_HOLD_LEN   10
#define FUTURE_LEN     (5*6)
#define NEXT_LEN       (1)
#endif

typedef struct {
	int score;
	int associated_future;
} next_score_t;

typedef struct {
	uint8 steps[FUTURE_LEN];
	int next_index;
	int tail_index;
	next_score_t score;
	int replace;
} future_t;



int get_overall_score(vector<uint8> &mem);
next_score_t score_next(future_t *fs, int future_number);

#endif /* SRC_PLAYFUN2_H_ */
